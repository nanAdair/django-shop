from django.conf.urls import patterns, url

urlpatterns = patterns("Products.views",
                       url(r'^(?P<slug>[-\w]*)$', "product_view", name="product"),
                       )
