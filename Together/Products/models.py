import uuid

from django.db import models
#from django.utils.translation import ugettext_lazy as _
#from django.contrib.contenttypes import generic

from Supplier.models import Supplier
from MainSite.models import User


def get_unique_id_str():
    return str(uuid.uuid4())


class Product(models.Model):

    """
    **Attibute:**
    name
        The name of the product

    slug
        Part of the URL

    price
        The gross price of the product

    short_description
        The short description of the product. This is used within overviews.

    description
        The description of the product. This is used within the detailed view
        of the product.

    images
        The images of the product.

    meta_title
        the meta title of the product (the title of the HTML page).

    meta_keywords
        the meta keywords of the product.

    meta_description
        the meta description of the product.

    active
        If False the product won't be displayed to shop users.

    creation_date
        The creation date of the product

    manage_stock_amount
        If true the stock amount of the product will be decreased when a
        product has been saled.

    supplier
        The supplier of the product

    uid
       The unique id of the product
    """
    name = models.CharField(max_length=80, blank=True)
    slug = models.SlugField(max_length=80, unique=True)
    uid = models.CharField(max_length=50, editable=False, unique=True, default=get_unique_id_str)
    price = models.FloatField(default=0.0)
    short_description = models.TextField(blank=True)
    description = models.TextField(blank=True)
    #images = generic.GenericRelation("Image", verbose_name=_(u"Images"),
                                     #object_id_field="content_id", content_type_field="content_type")
    #image = models.ImageField(upload_to="Image")

    meta_title = models.CharField(blank=True, default="<name>", max_length=80)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)

    active = models.BooleanField(default=False)
    creation_date = models.DateTimeField(auto_now_add=True)

    supplier = models.ForeignKey(Supplier, null=True, blank=True)
    manage_stock_amount = models.BooleanField(default=False)
    stock_amount = models.FloatField(default=100)

    #owner = models.ManyToManyField(User, null=True)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.slug)

    def decrease_stock_amount(self, amount):
        if self.manage_stock_amount:
            self.stock_amount -= amount
        self.save()

    def get_description(self):
        return self.description


class Image(models.Model):
    title = models.CharField(blank=True, max_length=100)
    product = models.ForeignKey(Product)
    image = models.ImageField(upload_to="Image")

    def __unicode__(self):
        return "%s" % (self.title)


class Product_owner(models.Model):
    product = models.ForeignKey(Product)
    owner = models.ForeignKey(User)
    stock_amount = models.FloatField()
