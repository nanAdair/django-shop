'''
Created on Apr 14, 2012  9:03:48 PM

@author:  Kangda Hu
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('MainSite.views.profile',
    url('^$', 'profile_detail', name='profile-detail'),
    url('^edit/$', 'profile_edit', name='profile-edit'),
)