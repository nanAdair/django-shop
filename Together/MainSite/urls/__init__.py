from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns('MainSite.views',
    url('^', include('MainSite.urls.common')),
    url('^profile/', include('MainSite.urls.profile')),
    url('^settings/', include('MainSite.urls.settings')),
    url('^users/',include('MainSite.urls.users')),
    url('^activities/', include('MainSite.urls.activities')),
    
)



#urlpatterns += staticfiles_urlpatterns()