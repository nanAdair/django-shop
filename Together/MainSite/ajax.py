'''
Created on Jun 6, 2012

@author: Kangda
'''
from datetime import datetime

from dajaxice.decorators import dajaxice_register #@UnresolvedImport
from dajax.core.Dajax import Dajax #@UnresolvedImport

from MainSite.views.activities import is_favourite #@UnresolvedImport
from MainSite.views.activities import update_popular #@UnresolvedImport
from MainSite.views.activities import modify_favourite #@UnresolvedImport
from MainSite.views.activities import has_voted #@UnresolvedImport
from MainSite.views.activities import vote as real_vote #@UnresolvedImport
from MainSite.views.activities import update_vote #@UnresolvedImport
from MainSite.views.activities import has_joined #@UnresolvedImport
from MainSite.models import User #@UnresolvedImport
from MainSite.models import Activity #@UnresolvedImport
from MainSite.models import Comment #@UnresolvedImport
from MainSite.models import ActivityMeta #@UnresolvedImport
from MainSite.models import UserMeta #@UnresolvedImport
from MainSite.utils import check_user #@UnresolvedImport
from MainSite.templatetags.gravatar import get_gravatar_for_email  #@UnresolvedImport
from MainSite.templatetags.gravatar import get_img_for_user #@UnresolvedImport
from MainSite.templatetags.user import get_fullname_for_country #@UnresolvedImport
from MainSite.settings import USERMETA_FOLLOW #@UnresolvedImport
from MainSite.settings import ACTMETA_LIKE #@UnresolvedImport
from MainSite.settings import ACTMETA_JOIN #@UnresolvedImport


@dajaxice_register
def vote(request, aid):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    if has_voted(user.id, aid):
        dajax.alert("You have voted already..")
    else:
        if real_vote(user.id, aid) == "Success":
            ##Update activity's vote/popular count.##
            v = update_vote(aid)
            dajax.assign('#vote-number', 'innerHTML', str(v))
            src = get_gravatar_for_email(user.email, 40, 'g')
            dajax.append('#avatar_list', 'innerHTML', 
                         '<a href="/users/{0.id}/"> \
                            <img alt="{0.nickname}" \
                            source="{1}" \
                            src="{1}" \
                            title="{0.nickname}" \
                            class="user_avatar_itm"> \
                          </a>'.format(user, src))
        else:
            dajax.alert("Vote Failed...")
            
    return dajax.json()


@dajaxice_register
def favourite(request, aid):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    if is_favourite(user.id, aid):
        
        if modify_favourite(user.id, aid, 'DOWN') == "Fail":
            dajax.alert("Modify Failed...")
        else:
            dajax.assign('#fav', 'src', '/static/together/img/unfollow.gif')
            dajax.assign('#fav', 'alt', 'UnMarked')
            dajax.assign('#fav', 'title', 'Mark')
    else:
        if modify_favourite(user.id, aid, 'UP') == "Fail":
            dajax.alert("Modify Failed...")
        else:
            dajax.assign('#fav', 'src', '/static/together/img/follow.gif')
            dajax.assign('#fav', 'alt', 'Marked')
            dajax.assign('#fav', 'title', 'UnMark')
    
    pop = update_popular(aid)
    dajax.assign('#fav-count', 'innerHTML', str(pop))
    
    return dajax.json()

@dajaxice_register
def join(request, aid):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    
    if has_joined(user.id, aid):
        dajax.alert("You have joined already..")
    elif Activity.is_alone_activity(aid):
        dajax.alert("The activity need no members.{0}".format(Activity.is_alone(aid)))
    else:
    
        ActivityMeta.create_meta_data(aid, user.id, ACTMETA_JOIN)
        dajax.append('#member_list', 'innerHTML', 
                     '<div class="cf-itm group-itm"> \
                        <div class="group-photo"> \
                            <a href="/users/{0.id}/"> \
                            {1} \
                            </a> \
                        </div> \
                        <div class="group-detail"> \
                            <h4 class="group-mem-name"> \
                            {0.nickname} \
                            </h4> \
                            <p class="group_desc"> \
                            {0.city}, {2} \
                            </p> \
                        </div> \
                    </div>'.format(user,
                                   get_img_for_user(user, 60, 'g'),
                                   get_fullname_for_country(user.country)))
    
    return dajax.json()

@dajaxice_register
def follow(request, sid):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    
    if UserMeta.has_meta_value(user.id, sid, USERMETA_FOLLOW):
        UserMeta.delete_meta_data(user.id, sid, USERMETA_FOLLOW)

        html_str = """<button class="btn btn-info" id="prof-follow" value="follow" 
                    onclick="Dajaxice.MainSite.follow(Dajax.process,{{ 'sid':{0} }});">
                        Follow
                    </button>""".format(sid)
        
        dajax.assign('#prof-follow-shell', 'innerHTML', html_str)
        
    else:
        UserMeta.create_meta_data(user.id, sid, USERMETA_FOLLOW)

        html_str = """<button class="btn btn-info disabled" id="prof-follow" value="unfollow" 
                    onclick="Dajaxice.MainSite.follow(Dajax.process,{{ 'sid':{0} }});">
                        Unfollow
                    </button>""".format(sid)
        dajax.assign('#prof-follow-shell', 'innerHTML', html_str)
    
    return dajax.json()


@dajaxice_register
def deletecomment(request, cid):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    try:
        cmt = Comment.objects.get(id=cid)
    except Comment.DoesNotExist:
#        tips = "Can't find the Activity object."
        dajax.alert("Can't find the Comment object.")
    else:
        cmt.delete()
        #dajax.alert("#cmt{0}".format(cid))
        dajax.remove("#cmt{0}".format(cid))
    
    return dajax.json()

@dajaxice_register
def activity_list(request, offset, order):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    act_start = offset
    act_end = act_start + 10
    
    if order is not None:
        if order == 'latest':
            acts = Activity.objects.all()[act_start:act_end]
        elif order == 'popular':
            acts = Activity.objects.order_by('-popular')[act_start:act_end]
        elif order == 'vote':
            acts = Activity.objects.order_by('-vote')[act_start:act_end]
        elif order == 'ongo':
            acts = Activity.objects.filter(start_time__lte=datetime.now)[act_start:act_end]
        elif order == 'notyet':
            acts = Activity.objects.filter(start_time__gt=datetime.now)[act_start:act_end]    
    else:
        order = 'latest'
        acts = Activity.objects.all()[act_start:act_end]
    
    htmlStr = ""
    for act in acts:
        creator = act.get_creator()
        htmlStr += '''
        <div class="act-itm">
            <a href="/users/{0.id}/" class="act-itm-photo" >
                <img alt="{0.nickname}" 
                  source="{2}"
                  src="{2}"
                  title="{0.nickname}">
            </a>
            <div class="act-itm-desc">
                <div class="act-itm-title">
                    <a href="/activities/{1}.id/">
                        <font size="3px">{1.title}</font>
                    </a>
                </div>
                <div class="act-itm-text">
                    <a href="/users/{0.id}/">{0.nickname}</a>
                    created at {1.creat_time}
                </div>
                <div class="act-itm-date">
                    Duration: {1.start_time} -- {3}
                </div>
            </div>
            <div class="act-itm-right">
                <h3>{1.vote} <small>Votes</small></h3>
            </div>
        </div>
        '''.format(
                creator,
                act,
                get_gravatar_for_email(creator.email, 50, 'g'),
                ('No ending', act.end_time)[act.has_endtime],)
    
    #dajax.alert("test")
    dajax.append('.act-inner', 'innerHTML', htmlStr)
    dajax.add_data({'count':len(acts)}, 'afterload')
    
    return dajax.json()


@dajaxice_register
def activity_fav(request, offset, user_id):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    if user_id is None:
        user_id = str(user.id)
        
    ###TOFIX!!!Code Below MAY Cause BUG!!!
    try: 
        User.objects.get(id=user_id)
    except User.DoesNotExist:
        dajax.redirect("/signin/")
        return dajax.json()
    ###TOFIX!!!
    
    act_start = offset 
    act_end = act_start + 10
    
#    acts = Activity.get_activities_by_ids(UserMeta.get_meta_value(user_id, ACTMETA_LIKE))[act_start:act_end]
    acts = Activity.get_activities_by_ids(ActivityMeta.get_activity_list(user_id, ACTMETA_LIKE))[act_start:act_end]
    
    htmlStr = ""
    for act in acts:
        creator = act.get_creator()
        htmlStr += """
        <div class="act-itm">
            <a href="/users/{0.id}/" class="act-itm-photo" >
                <img alt="{0.nickname}" 
                  source="{2}"
                  src="{2}"
                  title="{0.nickname}">
            </a>
            <div class="act-itm-desc">
                <div class="act-itm-title">
                    <a href="/activities/{1}.id/">
                        <font size="3px">{1.title}</font>
                    </a>
                </div>
                <div class="act-itm-text">
                    <a href="/users/{0.id}/">{0.nickname}</a>
                    created at {1.creat_time}
                </div>
                <div class="act-itm-date">
                    Duration: {1.start_time} -- {3}
                </div>
            </div>
            <div class="act-itm-right">
                <h3>{1.vote} <small>Votes</small></h3>
            </div>
        </div>
        """.format(
                creator,
                act,
                get_gravatar_for_email(creator.email, 50, 'g'),
                ('No ending', act.end_time)[act.has_endtime],)
    
    dajax.append('.act-inner', 'innerHTML', htmlStr)
    dajax.add_data({'count':len(acts)}, 'afterload')
    
    return dajax.json()
    
@dajaxice_register
def activity_join(request, offset, user_id):
    
    dajax = Dajax()
    
    (response, user, is_logged) = check_user(request, '/signin/') #@UnusedVariable
    if response is not None:
        dajax.redirect("/signin/")
        return dajax.json()
    
    if user_id is None:
        user_id = str(user.id)
        
    ###TOFIX!!!Code Below MAY Cause BUG!!!
    try: 
        User.objects.get(id=user_id)
    except User.DoesNotExist:
        dajax.redirect("/signin/")
        return dajax.json()
    ###TOFIX!!!
    
    act_start = offset
    act_end = act_start + 10
    
#    acts = Activity.get_activities_by_ids(UserMeta.get_meta_value(user_id, ACTMETA_JOIN))[act_start:act_end]
    acts = Activity.get_activities_by_ids(ActivityMeta.get_activity_list(user_id, ACTMETA_JOIN))[act_start:act_end]
    
    htmlStr = ""
    for act in acts:
        creator = act.get_creator()
        htmlStr += """
        <div class="act-itm">
            <a href="/users/{0.id}/" class="act-itm-photo" >
                <img alt="{0.nickname}" 
                  source="{2}"
                  src="{2}"
                  title="{0.nickname}">
            </a>
            <div class="act-itm-desc">
                <div class="act-itm-title">
                    <a href="/activities/{1}.id/">
                        <font size="3px">{1.title}</font>
                    </a>
                </div>
                <div class="act-itm-text">
                    <a href="/users/{0.id}/">{0.nickname}</a>
                    created at {1.creat_time}
                </div>
                <div class="act-itm-date">
                    Duration: {1.start_time} -- {3}
                </div>
            </div>
            <div class="act-itm-right">
                <h3>{1.vote} <small>Votes</small></h3>
            </div>
        </div>
        """.format(
                creator,
                act,
                get_gravatar_for_email(creator.email, 50, 'g'),
                ('No ending', act.end_time)[act.has_endtime],)
    
    dajax.append('.act-inner', 'innerHTML', htmlStr)
    dajax.add_data({'count':len(acts)}, 'afterload')
    
    return dajax.json()
    