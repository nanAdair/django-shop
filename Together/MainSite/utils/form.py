'''
Created on Apr 30, 2012  4:02:05 PM

@author:  Kangda Hu
'''

from django import forms
from tagging.forms import TagField #@UnresolvedImport

from MainSite.settings import COUNTRIES #@UnresolvedImport
from wmd.widgets import MarkDownInput #@UnresolvedImport

class SigninForm(forms.Form):
    """
    Form for login.
    """
    email = forms.EmailField(required=True, 
                             error_messages={'required':'Please enter the email address.',
                                             'invalid':'Please enter the valid email.'},
                             widget=forms.TextInput(attrs={'class':'input-large',
                                                           'style':'margin-top:10px;',
                                                           'placeholder':'Email Address',}))
    password = forms.CharField(required=True, 
                               error_messages={'required':'Please enter the password.'},
                               widget=forms.PasswordInput(attrs={'class':'input-large',
                                                                 'style':'margin-top:10px;',
                                                                 'placeholder':'Password',}))
    
    remember = forms.BooleanField(required=False)

    
class SignupForm(forms.Form):
    """
    Form for register.
    """
    nickname = forms.CharField(required=True,
                               label='NickName',
                               max_length=20,
                               min_length=1,
                               help_text=r'An alias for you. Less than 20 characters.',
                               error_messages={'required':'Please enter the nickname.'},
                               widget=forms.TextInput(attrs={'class':'input-large',}))
    
    email = forms.EmailField(required=True,
                             label='E-mail',
                             help_text=r'Email for login.',
                             error_messages={'required':'Please enter the email address.',
                                             'invalid':'Please enter the valid email.'},
                             widget=forms.TextInput(attrs={'class':'input-large',}))
    
    password = forms.CharField(required=True, 
                               label='Password',
                               max_length=30,
                               min_length=6,
                               help_text=r'6-30 characters.',
                               error_messages={'required':'Please enter the password.'},
                               widget=forms.PasswordInput(attrs={'class':'input-large',}))
    
    confirm_password = forms.CharField(required=True, 
                                       label='Confirm Password',
                                       help_text=r'Email for login.',
                                       error_messages={'required':'Please confirm the password.'},
                                       widget=forms.PasswordInput(attrs={'class':'input-large',}))
    
class ChgpwdForm(forms.Form):
    """
    Form for change password.
    """
    old_password = forms.CharField(required=True, 
                                   error_messages={'required':'Please enter the password.'},
                                   widget=forms.PasswordInput(attrs={'class':'input-large',
                                                                     'style':'margin-top:10px;',}))
    
    new_password = forms.CharField(required=True, 
                                   error_messages={'required':'Please enter the password.'},
                                   widget=forms.PasswordInput(attrs={'class':'input-large',
                                                                     'style':'margin-top:10px;',}))
    
    confirm_password = forms.CharField(required=True, 
                                       error_messages={'required':'Please enter the password.'},
                                       widget=forms.PasswordInput(attrs={'class':'input-large',
                                                                         'style':'margin-top:10px;',}))


class ProfileForm(forms.Form):
    """
    Form for Editint Profile.
    """
    nickname = forms.CharField(required=True,
                               label='NickName',
                               max_length=20,
                               min_length=1,
                               help_text=r'An alias for you. Less than 20 characters.',
                               error_messages={'required':'Please enter the nickname.'},
                               widget=forms.TextInput(attrs={'class':'input-large',
                                                             'style':'width:300px',}))
    
    country = forms.ChoiceField(required=False,
                                label='Country / Region',
                                choices=COUNTRIES)
    
    city = forms.CharField(required=False,
                           label='City', 
                           widget=forms.TextInput(attrs={'class':'input-large',
                                                         'style':'width:300px',}))
    
    about = forms.CharField(required=False, 
                            label='About Me',
                            widget=forms.Textarea(attrs={'style':'width:300px;height:80px',}))

class CommentForm(forms.Form):
    """
    Form for comment.
    """
    author = forms.CharField(required=True,
                             widget=forms.HiddenInput())
    
    activity = forms.CharField(required=True,
                               widget=forms.HiddenInput())
    
    content = forms.CharField(required=True,
                              widget=forms.Textarea(attrs={'style':'width:600px;height:80px',
                                                           'placeholder' : 'Leave a Comment here.'}))

class VoteForm(forms.Form):
    """
    Form for voting.
    """
    user = forms.CharField(required=True,
                           widget=forms.HiddenInput())
    
    activity = forms.CharField(required=True,
                               widget=forms.HiddenInput())

class ActivityForm(forms.Form):
    """
    Form for activity.
    """
    title = forms.CharField(required=True,
                            label=r'Title',
                            max_length=100,
                            min_length=1,
                            help_text=r'Name for your activity.',
                            widget=forms.TextInput(attrs={'class':'input-large',
                                                          'style':'width:80%',}))
    
    url = forms.CharField(required=False,
                          label='URL',
                          help_text=r'URL for your activity.',)
    
    news = forms.CharField(required=False,
                           help_text='Latest news about your activity.',
                           widget=forms.Textarea(attrs={'style':'width:80%;height:80px'}))
    
    content = forms.CharField(required=True,
                              label='Description',
                              widget=MarkDownInput(attrs={'style':'height:100px'}))
    
    is_alone = forms.BooleanField(required=False,
                                  label=r'Alone',
                                  help_text=r'Allow members or not?')
    
    location = forms.CharField(required=True,
                               label=r'Location')
    
    has_endtime = forms.BooleanField(required=False,
                                     label=r'Has Endtime',
                                     help_text=r'Time limited or not?')
    
    start_time = forms.DateTimeField(required=True,
                                     label=r'Start Time',
                                     widget=forms.DateTimeInput())
    
    end_time = forms.DateTimeField(required=False,
                                   label=r'End Time',
                                   widget=forms.DateTimeInput())
    
    tags = TagField(required=True,
                    label=r'Tags',
                    help_text=r'Separated by commas(,).')
    
class StatusForm(forms.Form):
    """
    Form for status.
    """
    content = forms.CharField(required=True,
                              widget=forms.Textarea(attrs={'style':'width:95%; min-height:80px;'}))


class MessageForm(forms.Form):
    """
    Form for message.
    """
    recipient = forms.ChoiceField(required=True,
                                  label='Recipient',
                                  widget=forms.Select(attrs={'data-placeholder':'Choose a Recipient...',
                                                             'class':'chzn-select',
                                                             'style':'width:350px;'}))
    content = forms.CharField(required=True,
                              label='Content',
                              widget=forms.Textarea(attrs={'style':'width:400px;min-height:80px;'}))
    
    def set_recipients(self, choices):
        self.fields['recipient']._set_choices(choices)
    
    
