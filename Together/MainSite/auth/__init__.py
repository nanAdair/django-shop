
from MainSite.auth.backends import ModelBackend #@UnresolvedImport

SESSION_KEY = '_auth_user_id'


def authenticate(**credentials):
    """
    If the given info are valid, return the user object.
    """
    backend = ModelBackend()
    try:
        user = backend.authenticate(**credentials)
    except TypeError:
        pass
    
    if user is None:
        pass
    return user

def login(request, user):
    """
    Handle with login operation. Add user's id to session.
    """
    if user is None:
        user = request.user
    
    if SESSION_KEY in request.session:
        if request.session[SESSION_KEY] != user.id:
            request.session.flush()
    else:
        request.session.cycle_key()
    
    request.session[SESSION_KEY] = user.id
    
    if hasattr(request, 'user'):
        request.user = user

def logout(request):
    """
    Remove the id from session.
    """
    request.session.flush()
    
    if hasattr(request, 'user'):
        from MainSite.models import AnonymousUser #@UnresolvedImport
        request.user = AnonymousUser()

def get_user(request):
    from MainSite.models import AnonymousUser #@UnresolvedImport
    try:
        user_id = request.session[SESSION_KEY]
        backend = ModelBackend()
        user = backend.get_user(user_id) or AnonymousUser()
    except KeyError:
        user = None
    return user