from django.db import models
from MainSite.models import User

# Create your models here.


class Supplier(models.Model):

    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True, max_length=80)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s" % (self.name)
